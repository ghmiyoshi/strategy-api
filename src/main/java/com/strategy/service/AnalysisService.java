package com.strategy.service;

import static com.strategy.strategy.Type.ANALYSIS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.strategy.strategy.AnalysisContext;

@Service
public class AnalysisService {

	@Autowired
	private AnalysisContext analysisContext;

	public void analysis() {
		analysisContext.execute(ANALYSIS);
	}

}
