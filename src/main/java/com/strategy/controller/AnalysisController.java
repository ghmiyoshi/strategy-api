package com.strategy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.strategy.service.AnalysisService;

@RestController
@RequestMapping("/analysis")
public class AnalysisController {

	@Autowired
	private AnalysisService analysisService;

	@GetMapping
	public void analysis() {
		analysisService.analysis();
	}

}
