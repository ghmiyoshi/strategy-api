package com.strategy.strategy;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.stereotype.Component;

@Component
public class AnalysisContext {

	@Autowired
	private BeanFactory bean;

	public void execute(Type type) {
		AnalysisStrategy strategy = BeanFactoryAnnotationUtils.qualifiedBeanOfType(bean, AnalysisStrategy.class, type.name());
		strategy.execute();
	}

}
