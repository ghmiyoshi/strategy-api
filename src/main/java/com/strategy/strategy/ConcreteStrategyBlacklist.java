package com.strategy.strategy;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("BLACKLIST")
@Component
public class ConcreteStrategyBlacklist extends AnalysisStrategy {

	@Override
	protected void reprove() {
		System.out.println("Reprove - ConcreteStrategyBlacklist");
	}

	@Override
	protected void aprove() {
		System.out.println("Aprove - ConcreteStrategyBlacklist");
	}

	@Override
	protected boolean mustAply() {
		return true;
	}

}
