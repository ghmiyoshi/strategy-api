package com.strategy.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class AnalysisStrategy {

	@Autowired
	protected AnalysisContext analysisContext;

	/* Template Method */
	public void execute() {
		if (mustAply()) {
			aprove();
		} else {
			reprove();
		}
	}

	protected abstract void reprove();
	protected abstract void aprove();
	protected abstract boolean mustAply();

}
