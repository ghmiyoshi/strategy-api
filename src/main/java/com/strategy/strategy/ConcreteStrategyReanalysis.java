package com.strategy.strategy;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("REANALYSIS")
@Component
public class ConcreteStrategyReanalysis extends AnalysisStrategy {

	@Override
	protected void reprove() {
		System.out.println("Reprove - ConcreteStrategyReanalysis");
	}

	@Override
	protected void aprove() {
		System.out.println("Aprove - ConcreteStrategyReanalysis");
	}

	@Override
	protected boolean mustAply() {
		return false;
	}

}
