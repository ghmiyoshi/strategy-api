package com.strategy.strategy;

import lombok.Getter;

@Getter
public enum Type {

	ANALYSIS,
	BLACKLIST,
	EXCLUSION,
	REANALYSIS;

}
