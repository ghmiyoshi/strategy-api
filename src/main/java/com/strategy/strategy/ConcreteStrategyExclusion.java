package com.strategy.strategy;

import static com.strategy.strategy.Type.*;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("EXCLUSION")
@Component
public class ConcreteStrategyExclusion extends AnalysisStrategy {

	@Override
	protected void reprove() {
		System.out.println("Reprove - ConcreteStrategyExclusion");
		analysisContext.execute(REANALYSIS);
	}

	@Override
	protected void aprove() {
		System.out.println("Aprove - ConcreteStrategyExclusion");
		analysisContext.execute(BLACKLIST);
	}

	@Override
	protected boolean mustAply() {
		return true;
	}

}
