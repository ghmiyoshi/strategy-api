package com.strategy.strategy;

import static com.strategy.strategy.Type.*;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("ANALYSIS")
@Component
public class ConcreteStrategyAnalysis extends AnalysisStrategy {

	@Override
	protected void reprove() {
		System.out.println("Reprove - ConcreteStrategyAnalysis");
		analysisContext.execute(EXCLUSION);
	}

	@Override
	protected void aprove() {
		System.out.println("Aprove - ConcreteStrategyAnalysis");
		analysisContext.execute(BLACKLIST);
	}

	@Override
	protected boolean mustAply() {
		return false;
	}

}
